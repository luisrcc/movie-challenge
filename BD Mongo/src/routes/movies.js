import { Router } from 'express';
const router = Router();

import { connect } from "../database";
import { ObjectID } from "mongodb";

router.get('/', async (req, res) =>{
    const db = await connect();
    const result = await db.collection('movie'.find({}).toArray());
    res.json(result);
});

router.get('/:id', async (req, res) =>{
    const { id } = req.params;
    const db = await connect();
    const result = await db.collection('movie').findOne({_id: ObjectID(id) });
});

router.post('/', async (req, res) =>{
    const db = await db.collection();
    const movie = {
        title: req.body.title
    };
    const result = await db.collection('movie').insert(movie);
    res.json(result.ops[0]);
});