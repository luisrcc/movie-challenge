import React from "react";
import ReactDOM from "react-dom";
import List from "./store/List";
import "./index.css";
import NavBar from './components/NavBar';
import "bootswatch/dist/lux/bootstrap.min.css";

const App = () => {
  return (
    <>
    <NavBar />
      <main>
        <div className="container">
          <List />
        </div>
      </main>
    </>
  );
};

ReactDOM.render(<App />, document.getElementById("root"));
