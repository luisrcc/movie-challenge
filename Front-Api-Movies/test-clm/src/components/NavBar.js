import React from "react";

const NavBar = () => {
  return (
    <nav className="navbar navbar-dark bg-dark border-bottom border-white">
      <div className="container">
        <a className="navbar-brand" href="/">
          MovieChallenge
        </a>
      </div>
    </nav>
  );
};

export default NavBar;